# random-quote-machine
FCC - Random Quote Machine <br>
<b>Work Completed random-quote-machine</b>


## This release contains the FF enhancement:

* Improvements on <code>head</code> tag
* <code>tweetMessage</code> variable has been defined
* Implemented <code>alert()</code> to disable twitter share if <code>newQuote()</code> is not yet called

### Lessons Learned

* Using the Chrome Developer tools in manipulating the DOM
* JavaScript Arrays
* <a href="https://developer.mozilla.org/en-US/docs/Web/API/Node/textContent">node.textContent</a> property
* Adding <a href="https://dev.twitter.com/web/tweet-button">Tweet button</a>


## See it Live here: https://kbpsystem777.github.io/random-quote-machine/index.html


#### Special Thanks to <a href="https://forum.freecodecamp.org/">freecodecamp community!</a>
